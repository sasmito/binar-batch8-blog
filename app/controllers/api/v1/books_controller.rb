class Api::V1::BooksController < ApplicationController
  skip_before_action :verify_authenticity_token
  before_action :find_book, only: [:update, :show, :destroy]

  def index
    books = Book.all
    @response = [:ok, books, nil]
    render status: :ok
  end
  
  def show
    render json: { status: :ok, result: @book.blank? ? ["books not found"]  : @book, error: nil }  
  end
  
  def create
    book = Book.new(book_params)
    if book.save
      render json: { status: :created, result: book, error: nil }  
    else
      render json: { status: :unprocessable_entity, result: ["failed create new book"] , error: nil }
    end
  end
  
  def update
    if @book 
      @book.update(book_params)
      if @book.save
        render json: { status: :ok, result: @book, error: nil }
      else  
        render json: { status: :unprocessable_entity, result: nil, error: ["Failed to update book"] }  
      end
    end
  end
  
  def destroy
    if @book
      @book.destroy!
      render json: { status: :ok, result: ["success to delete book"], error: nil }  
    else
      render json: { status: :unprocessable_entity, result: nil, error: ["Failed to destroy book"] }  
    end
  end
  
  private
  
  def find_book
    @book = Book.find_by(id: params[:id])
  end

  def book_params
    params.require(:book).permit(:author_id, :name, :published_at)
  end


end
