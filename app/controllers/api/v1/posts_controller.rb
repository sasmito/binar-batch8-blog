class Api::V1::PostsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :find_post, only: [:update, :show, :destroy]

    def index
        posts = Post.all
        render json: { status: :ok, result: posts.blank? ? ["posts not found"]  : posts, error: nil }
    end

    def show
        render json: { status: :ok, result: @post.blank? ? ["posts not found"]  : @post, error: nil }        
    end

    def create
        post = Post.new(post_params)
        if post.save
        render json: { status: :created, result: post, error: nil }  
        else
        render json: { status: :unprocessable_entity, result: ["failed create new post"] , error: nil }
        end        
    end

    def update
        if @post 
          @post.update(post_params)
          if @post.save
            render json: { status: :ok, result: @post, error: nil }
          else  
            render json: { status: :unprocessable_entity, result: nil, error: ["Failed to update post"] }  
          end
        end
      end

      def destroy
        if @post
          @post.destroy!
          render json: { status: :ok, result: ["success to delete post"], error: nil }  
        else
          render json: { status: :unprocessable_entity, result: nil, error: ["Failed to destroy post"] }  
        end
      end

    private
  
    def find_post
        @post = Post.find_by(id: params[:id])
    end

    def post_params
        params.require(:post).permit(:body, :author_id, :category_id)
    end
    
    
    
end