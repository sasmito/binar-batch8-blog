class Api::V1::AuthorsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :find_author, only: [:update, :show, :destroy]
  
    def index
       author= Author.all
      render json: { status: :ok, result: author.blank? ? ["author not found"]  : author, error: nil }  
    end
    
    def show
      render json: { status: :ok, result: @author.blank? ? ["author not found"]  : @author, error: nil }  
    end
    
    def create
      author = Author.new(author_params)
      if author.save
        render json: { status: :created, result: author, error: nil }  
      else
        render json: { status: :unprocessable_entity, result: ["failed create new author"] , error: nil }
      end
    end
    
    def update
      if @author 
        @author.update(author_params)
        if @author.save
          render json: { status: :ok, result: @author, error: nil }
        else  
          render json: { status: :unprocessable_entity, result: nil, error: ["Failed to update author"] }  
        end
      end
    end
    
    def destroy
      if @author
        @author.destroy!
        render json: { status: :ok, result: ["success to delete author"], error: nil }  
      else
        render json: { status: :unprocessable_entity, result: nil, error: ["Failed to destroy author"] }  
      end
    end
    
    private
    
    def find_author
      @author = Author.find_by(id: params[:id])
    end
  
    def author_params
      params.require(:author).permit(:name, :twitter, :gitlab, :about)
    end
  
  
  end
  
