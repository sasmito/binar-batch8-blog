class Author < ApplicationRecord
  has_many :books 

  validates :name, presence: true
  validates :gitlab, presence: true

end
