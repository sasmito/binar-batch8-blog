json.status @response.first
json.result do
  json.array! @response.second do |book|
    json.except! book, :author_id
    json.author book.author
  end
end
json.error  @response.third
