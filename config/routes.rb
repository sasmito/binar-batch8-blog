Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"
  
  namespace :api do
    namespace :v1 do
      get 'books', to: 'books#index'
      get 'books/:id', to: 'books#show'
      post "books/create", to: "books#create"
      put "books/:id/update", to: "books#update"
      delete "books/:id/destroy", to: "books#destroy"

      get 'posts', to: 'posts#index'
      get 'posts/:id', to: 'posts#show'
      post 'posts/create', to: 'posts#create'
      put 'posts/:id/update', to: 'posts#update'
      delete 'posts/:id/destroy', to: 'posts#destroy'

      get 'authors', to: 'authors#index'
      get 'authors/:id', to: 'authors#show'
      post 'authors/create', to: 'authors#create'
      put 'authors/:id/update', to: 'authors#update'
      delete 'authors/:id/destroy', to: 'authors#destroy'
    end
  end
end
