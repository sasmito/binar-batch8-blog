# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Ruby version
- Ruby 2.5.0 using rvm
- Install gem bundle

## Setting Environment
* Install rvm from [here](https://rvm.io/rvm/install)
* Install postgresql
`$ sudo apt install libpq-dev postgresql postgresql-contrib`
* Install javascript runtime (eg. nodejs8)
* Install redis-server for session store

## Setup
* Clone repo
* Run bundle install
* Run postgresql
* Run `rails s`

## Materi
# TDD
* https://github.com/muhammadsasmito/rails-workshop
* https://github.com/rspec/rspec-rails
* https://github.com/colszowka/simplecov
* https://github.com/thoughtbot/factory_bot_rails
* https://github.com/thoughtbot/factory_bot/blob/master/GETTING_STARTED.md
# Clean Code
* https://github.com/rubocop-hq/ruby-style-guide
* https://github.com/uohzxela/clean-code-ruby

SDLC