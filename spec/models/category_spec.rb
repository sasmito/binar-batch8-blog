require "rails_helper"

RSpec.describe Category, :type => :model do
    it "validates presence of name" do
        category = FactoryBot.build :category, name: nil
        expect(category.valid?).to be_falsy
    end

    it "validate presence of name" do
        category = Category.new
        category.name = "apa"
        category.save
        expect(category.valid?).to be_falsy
    end

end
