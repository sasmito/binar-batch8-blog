require "rails_helper"

RSpec.describe Author, :type => :model do
  it "validate presence of name" do
    author = FactoryBot.build :author, name: nil
    expect(author.valid?).to be_falsy
  end
  
  it "validate presence of name" do
    author = Author.new
    author.gitlab = "@sasmito"
    author.save
    expect(author.valid?).to be_falsy 
  end
  
  it "validate presence of gitlab account" do
    author = Author.new
    author.name = "sasmito"
    author.save
    expect(author.valid?).to be_falsy 
  end
  
  it "validate presence of name and gitlab account" do
    author = Author.new
    author.save
    expect(author.valid?).to be_falsy 
  end
end
