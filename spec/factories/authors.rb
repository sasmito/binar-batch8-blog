FactoryBot.define do
  factory :author do
    name { "Muhammad Sasmito" }
    gitlab { "@sasmito" }

    about { "#{SecureRandom.hex(100)}" }

  end
end

